#include "stm32f10x.h"                  // Device header
#include "MPU6050.h"
#include "Timer.h"
#include "OLED.h"
#include "IMU.h"

int main(void)
{
	OLED_Init();
	MPU6050_Init();
	Timer_Init();
	
	while (1)
	{
		OLED_ShowString(1, 1, "pitch:");
		OLED_ShowString(2, 1, "roll :");
		OLED_ShowString(3, 1, "yaw  :");
		OLED_ShowFloatNum(1, 7, imu_Angle.Pitch, 6);
		OLED_ShowFloatNum(2, 7, imu_Angle.Roll , 6);
		OLED_ShowFloatNum(3, 7, imu_Angle.Yaw  , 6);
	}
}

void TIM2_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
	{
		IMU_getEuleranAngles();
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	}
}
